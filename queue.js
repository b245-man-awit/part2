let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    console.log(collection);
    return collection
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    collection[collection.length] = element
    return collection
}


function dequeue() {
    // In here you are going to remove the first element in the array
    let newCollection = []
    let removedCollection = []
    for (let i = 0; i < collection.length; i++) {
        const item = collection[i];
        if (i === 0) {
            removedCollection = [
                ...removedCollection,
                item
            ]
        } else {
            newCollection = [
                ...newCollection,
                item
            ]
        }
    }

    collection = newCollection
    return collection
}


function front() {
    // In here, you are going to get the first element
    return collection[0]
}

// starting from here, di na pwede gumamit ng .length property
function size() {
    // Number of elements   
    let index = 0;
    while (collection[index] !== undefined) {
        index++
    }
    return index
}

function isEmpty() {
    //it will check whether the function is empty or not
    let empty = false
    for (let i = 0; i < size(); i++) {
        if (collection[i] === null) {
            empty = true
            return empty
        }
    }

    return empty
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};